SRC	=	src/main.c \
		src/mt.c \

OBJ	=	$(SRC:.c=.o)

NAME	=	tp3

all:	$(NAME)

$(NAME):
	gcc $(SRC) -Wall -v -I src/include -o $(NAME) -g -lm

clean:
	rm -f $(OBJ)
	rm -rf *.dSYM

fclean:	clean
	rm -f $(NAME)

re:	fclean all
run:
	gcc $(SRC) -Wall -v -I src/include -o $(NAME) -g -lm
	./$(NAME)

.PHONY:	all clean fclean re