
## Pour commencer

Placez vous dans le dossier du projet avec la commande cd

### Makefile

Le makefile est un fichier qui permet de compiler et d'executer le projet en une seule commande.

### Installation

1 - Lancer un terminal dans le dossier du projet
Executez la commande `make run`, cette derniere lancera le programme.

## Fabriqué avec

- [C](https://devdocs.io/c/) - C est un langage de programmation impératif généraliste, de bas niveau.
- [Mersenne Twister](http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/MT2002/emt19937ar.html) - Mersenne Twister est un générateur de nombres pseudo-aléatoires.
- [Makefile](https://www.gnu.org/software/make/manual/make.html) - Makefile est un outil de construction de logiciels.

## Versions

**Dernière version stable :** 1.0
Liste des versions : [Cliquer pour afficher](https://gitlab.isima.fr/mwabichou/simpi.git)

## Auteurs

- **Mohamed Wassim Abichou** _alias_ [@mwabichou](https://gitlab.isima.fr/mwabichou)

Lisez la liste des [contributeurs](https://gitlab.isima.fr/mwabichou/simpi/-/project_members) pour voir les personnes qui ont participé au projet.

## License

Ce projet est sous la licence `AMW`
