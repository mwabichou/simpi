#include "lib.h"
/*----------------------------------------------*/
/*                                              */
/* Nom : menu                                   */
/*                                              */
/*----------------------------------------------*/
void menu()
{
    int choix, nbSim, nbPoints;
    system("clear");
    do
    {
        printf("\t\t------------------MENU------------------ \n");
        printf("\t\t1. Simulation de Pi \n");
        printf("\t\t2. Intervale de confiance \n");
        printf("\t\t0. Quitter \n");
        printf("Votre choix: ->");
        scanf("%d", &choix);
        switch (choix)
        {
        case 1:
            fflush(stdin);
            printf("Entrez le nombre de simulations: ");
            scanf("%d", &nbSim);
            printf("Entrez le nombre de points: ");
            scanf("%d", &nbPoints);
            printf("    \n");
            simPi_drawing(nbPoints, nbSim);
            printf("    \n");
            break;
        case 2:
            fflush(stdin);
            printf("Entrez le nombre de simulations: ");
            scanf("%d", &nbSim);
            printf("Entrez le nombre de points: ");
            scanf("%d", &nbPoints);
            printf("    \n");
            confidence_interval(nbSim, nbPoints);
            printf("    \n");
            break;
        case 0:
            printf("Au revoir ! \n");
            break;
        default:
            printf("Choix invalide \n");
            break;
        }
    } while (choix != 0);
}

/*----------------------------------------------*/
/*                                              */
/* Nom : simPi                                  */
/*                                              */
/* Entrées : Nombre de simulations              */
/*                                              */
/* Sorties : un float Pi                        */
/*                                              */
/*----------------------------------------------*/

double simPi(int nbPoints)
{
    int i;
    double x, y, pi;
    double count = 0;
    for (i = 0; i < nbPoints; i++)
    {
        x = 2 * genrand_real1() - 1;
        y = 2 * genrand_real1() - 1;
        if (x * x + y * y <= 1)
            count++;
    }
    pi = count / nbPoints * 4;
    return pi;
}

/*----------------------------------------------*/
/*                                              */
/* Nom : simPi_drawing                          */
/*                                              */
/* Entrées : Nombre de simulations              */
/*           Nombre de points                   */
/*                                              */
/* Sorties : aucune                             */
/*                                              */
/*----------------------------------------------*/

void simPi_drawing(int nbPoints, int nbSim)
{
    double *piTab = (double *)malloc(nbSim * sizeof(double));
    double meanPi = 0;
    int i;
    for (i = 0; i < nbSim; i++)
    {
        piTab[i] = simPi(nbPoints);
        meanPi += piTab[i];
    }
    meanPi /= nbSim;
    printf("Apres %d simulations de %d points, on obtient:\n", nbSim, nbPoints);
    printf("Moyenne empirique: %.20lf\n", meanPi);
    printf("Erreur absolue: %.20lf\n", fabs(meanPi - M_PI));
    printf("Erreur relative: %.20lf\n", fabs(meanPi - M_PI) / M_PI);
}
/*----------------------------------------------*/
/*                                              */
/* Nom : confidence_interval                    */
/*                                              */
/* Entrées : Nombre de simulations              */
/*           Nombre de points                   */
/*                                              */
/* Sorties : aucune                             */
/*                                              */
/*----------------------------------------------*/
void confidence_interval(int nbSim, int nbPoints)
{
    double *piTab = (double *)malloc(nbSim * sizeof(double)), tTab[30] = {63.657, 9.925, 5.841, 4.604, 4.032, 3.707, 3.499, 3.355, 3.25, 3.169, 3.106, 3.055, 3.012, 2.977, 2.947, 2.921, 2.898, 2.878, 2.861, 2.845, 2.831, 2.819, 2.807, 2.797, 2.787, 2.779, 2.771, 2.763, 2.756, 2.75};
    double meanPi = 0, t = 0;
    int i;
    for (i = 0; i < nbSim; i++)
    {
        piTab[i] = simPi(nbPoints);
        meanPi += piTab[i];
    }
    meanPi /= nbSim;
    if (nbSim < 30)
        t = tTab[nbSim - 1];
    else
        t = 2.576;
    double stdDev = 0;
    for (i = 0; i < nbSim; i++)
    {
        stdDev += (piTab[i] - meanPi) * (piTab[i] - meanPi);
    }
    stdDev /= nbSim - 1;
    stdDev = sqrt(stdDev) * t / sqrt(nbSim);
    printf("Apres %d simulations de %d points, on obtient:\n", nbSim, nbPoints);
    printf("Moyenne empirique: %.20lf\n", meanPi);
    printf("Erreur absolue: %.20lf\n", fabs(meanPi - M_PI));
    printf("Erreur relative: %.20lf\n", fabs(meanPi - M_PI) / M_PI);
    printf("Ecart type: %.20lf\n", stdDev);
    printf("Intervalle de confiance a 99%%: [%.20lf, %.20lf]\n", meanPi - stdDev, meanPi + stdDev);
}
int main(int argc, char *argv[])
{
    unsigned long init[4] = {0x123, 0x234, 0x345, 0x456}, length = 4;
    init_by_array(init, length);
    menu();

    // NE PAS DECOMMENTER CETTE PARTIE SAUF POUR TESTER LE NOMBRE DE SIMULATIONS NECESSAIRES POUR ATTEINDRE UNE PRECISION DONNEE
    // int i = 1;
    //      double precision = 0.01;
    //      while (fabs(simPi(10000000) - M_PI) > precision)
    //      {
    //          i++;
    //      }
    //      printf("precision %lf obtenu en %d simulations\n", precision, i);
    //      precision = 0.001;
    //      while (fabs(simPi(10000000) - M_PI) > precision)
    //      {
    //          i++;
    //      }
    //      printf("precision %lf obtenu en %d simulations\n", precision, i);
    //      precision = 0.0001;
    //      while (fabs(simPi(10000000) - M_PI) > precision)
    //      {
    //          i++;
    //      }
    //      printf("precision %lf obtenu en %d simulations\n", precision, i);
    //      precision = 0.00001;
    //      while (fabs(simPi(10000000) - M_PI) > precision)
    //      {
    //          i++;
    //      }
    //      printf("precision %lf obtenu en %d simulations\n", precision, i);
    //      precision = 0.000001;
    //      while (fabs(simPi(10000000) - M_PI) > precision)
    //      {
    //          i++;
    //      }
    //      printf("precision %lf obtenu en %d simulations\n", precision, i);
    return 0;
}
